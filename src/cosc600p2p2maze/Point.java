/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cosc600p2p2maze;

public class Point {
private int row;
private int col;
  
public Point(){
  
}
  
public Point(int x, int y){
this.row = x;
this.col = y;
}
/** para return the row */
public int getRow() {
return row;
}
/** param row the row to set */
public void setRow(int row) {
this.row = row;
}
/** param to return the col */
public int getCol() {
return col;
}
/** param col the col to set */
public void setCol(int col) {
this.col = col;
}
  
public void setPoint(int row,int col){
this.row = row;
this.col = col;
}
 
}  // end Point class