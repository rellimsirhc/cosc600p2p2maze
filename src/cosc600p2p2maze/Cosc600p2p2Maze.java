/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
*
 * Author: Christine Miller-lane
 * * Created on March 10, 2021, 1:09 PM
 * Given the starting point in a maze, you are to find and mark a path out of the 
 * maze is represented by a 20x20 array of 1s(representing hedges) and 0s(representing
 *  the foot-paths). There is only one exit from the maze(represented by E). You may
 *  move vertically or horizontally in any direction that contains a 0; you may not 
 * move into a square with a 1. If you move into the square with an E, you have exited
 *  the maze. If you are in a square with 1s on three sides, you must go back the way
 *  you came and try another path. You may not move diagonally.
 * For this program, a stack class should be used.
 * Input: Input is a 20x20 array of characters (1s, 0s, and E) from an ASCII text data
 *  file(maze.txt); for example:
 * Each data line consists of one row of maze. Starting points (i.e. a row, column pair)
 *  in the maze will be input from the keyboard.
 * Output: Echo print the maze complete with numbered rows and columns prior to asking the 
 * user for their starting point. For each entry into the maze, print the complete maze
 * with an S in the starting point followed by the words ‘I am free’ if you have found a 
 * path out of the maze or the words ‘Help, I am trapped’ if you cannot. You must also
 *  print the path (by using a series of pluses(+)) you took through the maze should one
 *  be found.  Here is a summary of algorithm:
 * 1. At the beginning square we will examine the four adjacent squares and push onto the move stack the ones with a 0 or E in them.
 * 2. In order to move, pop one square from the stack (Stack1). If stack is empty (when this happens it means that you are surrounded by 1s and/or +s), you are trapped. Mark the square we are in as having been visited by putting a + in the square.
 * 3. Get the next move from the stack and make the square whose coordinates have just been popped the current square.
 * 4. Repeat the process until you either reach the exit point or try to back track and the stack
 * 5. Note that the above algorithm does not explain how one determines and marks the path to the exit from the starting point.
 */

package cosc600p2p2maze;

import java.io.*;
import java.util.*;

public class Cosc600p2p2Maze {

static int maxRow = 19;
static int minRow = 0;
static int maxCol = 19;
static int minCol = 0;
public static void main(String[] args) throws FileNotFoundException, IOException {
// TODO code application logic here

Scanner console = new Scanner(System.in);
char[][] maze = new char[20][20];
maze = readFile("maze.txt");
int rowStart = getRowStart();
int colStart = getColStart();

// verify valid input to start
while(maze[rowStart][colStart] == '1' || maze[rowStart][colStart] == 'E'){
    System.out.println("Invalid starting point,please try again");
    rowStart = getRowStart();
    colStart = getColStart();
}
// start of print for I am free with 's'
System.out.println();
maze[rowStart][colStart] = 'S';
solveMaze(maze,rowStart,colStart);
printMaze(maze);
}
public static void printMaze(char[][] maze){     //print the maze
    for(int row = 0; row < 20; row++){
        System.out.println("");
        for (int col = 0; col <20; col ++){
            System.out.print(maze[row][col]);
        }  // end for
    } // end for
    System.out.println();
}  // end print maze
    
public static char[][] readFile(String file) throws FileNotFoundException{
    char[][] maze = new char[20][20];
    Scanner console = new Scanner(new File(file));
    for(int row = 0; row<20;row++)
    {
        String line = console.next();
        System.out.println(line);
        for(int col=0;col<20;col++){
            maze[row][col] = line.charAt(col);
        }
    }
    return maze;
}
// for those of you who choose to read the file in as an array of characters.
//You want to omit '/r' and '/n' which is a carriage return and new line character respectively.

public static int getRowStart(){        //get starting row
    boolean flag = false;
    int rowStart = 0;
    System.out.println("Which row would you like to start at(0-19)");
    rowStart = getInt();
    while ((rowStart < minRow) | (rowStart > maxRow)){
        flag = false;
        System.out.println("Enter a valid row to start at(0-19)");
        rowStart = getInt();
    }
return rowStart;
}  //end getRowStart  to get starting row

public static int getColStart(){    //getting starting column
    boolean flag = false;
    Scanner console = new Scanner(System.in);
    System.out.println("Which col would you like to start at(0-19)");
    int colStart = getInt();
    while ((colStart<minRow) || (colStart>maxCol)){
        flag = false;
        System.out.println("Enter a valid col to start at(0-19)");
        colStart = getInt();
    }
    return colStart;
} // end of getColStart for getting starting column

public static int getInt(){
    Scanner inp = new Scanner(System.in);
    int intInp = 0;
    boolean valid;
    do{
        if(inp.hasNextInt()){
            intInp = inp.nextInt();
            valid = true;
        }
        else {
            valid = false;
            inp.next();
            System.out.println("Not a valid integer! Please try again!");   
        }
    }
    while (!valid);
    return intInp;
}

// solving maze
public static boolean solveMaze(char maze[][],int row,int col){
    boolean solved = false;
    Point p = new Point(row,col);
    Point cur = null;                       // current point
    Stack<Point> s = new Stack<Point>();
    s.push(p);
    while(!s.isEmpty() && solved == false){
        cur = s.pop();
        if(maze[cur.getRow()][cur.getCol()] == 'E'){
            solved = true;
            System.out.println("");
            System.out.println("I am free");
        }
        else{
            if(isValidUp(maze,cur.getRow(),cur.getCol()) == true)  
                s.push(new Point(cur.getRow()-1, cur.getCol()));
            if(isValidRight(maze,cur.getRow(),cur.getCol()) == true)
                s.push(new Point(cur.getRow(),cur.getCol()+1));
            if(isValidDown(maze,cur.getRow(),cur.getCol()) == true)
                s.push(new Point(cur.getRow()+1,cur.getCol()));
            if(isValidLeft(maze,cur.getRow(),cur.getCol()) == true)
                s.push(new Point(cur.getRow(),cur.getCol()-1));
        }
        if(maze[cur.getRow()][cur.getCol()] != 'E' && maze[cur.getRow()][cur.getCol()] != 'S')
            maze[cur.getRow()][cur.getCol()] = '+';
        if(s.isEmpty()){
            System.out.println();
            System.out.println("I am trapped");
        }
    }
    return solved;   
}  // end solveMaze

// validate positions or direction to move  
/* At the beginning square we will examine the four adjacent squares and push 
* onto the move stack the ones with a 0 or E in them. */
public static boolean isValidUp(char[][] maze,int row, int col){
    boolean isValid = false;
    if (row-1 < minRow)
        isValid = false;
    else if((maze[row-1][col] == '0') ||(maze[row-1][col] == 'E') )
        isValid = true;
    return isValid;
}  // end isValidUp

public static boolean isValidRight(char[][] maze,int row, int col){
    boolean isValid = false;
    if (col+1 > maxCol)
        isValid = false;
    else if((maze[row][col+1] == '0') ||(maze[row][col+1] == 'E'))
        isValid = true;
    return isValid;
}  // end isValidRight

public static boolean isValidDown(char[][] maze,int row, int col){
    boolean isValid = false;
    if (row+1 > maxRow)
        isValid = false;
    else if((maze[row+1][col] == '0') ||(maze[row+1][col] == 'E') )
        isValid = true;
    return isValid;
}  // end isValidDown

public static boolean isValidLeft(char[][] maze,int row, int col){
    boolean isValid = false;
    if (col-1 < minCol)
        isValid = false;
    else if((maze[row][col-1] == '0') ||(maze[row][col-1] == 'E') )
        isValid = true;
    return isValid;
}  //end isValidLeft




} //end Cosc600p2p2Maze




/**
     * @param args the command line arguments
     
    public static void main(String[] args) {
        // TODO code application logic here
    }*/
    
