# Cosc600p2p2Maze

Given the starting point in a maze,  find and mark a path out of the maze is represented by a 20x20 array of 1s(representing hedges) and 0s(representing  the foot-paths). There is only one exit from the maze(represented by E). 

Given the starting point in a maze, you are to find and mark a path out of the maze is represented by a 20x20 array of 1s(representing hedges) and 0s(representing the foot-paths). There is only one exit from the maze(represented by E). You may move vertically or horizontally in any direction that contains a 0; you may not move into a square with a 1. If you move into the square with an E, you have exited the maze. If you are in a square with 1s on three sides, you must go back the way you came and try another path. You may not move diagonally.
For this program, a stack class should be used.
Input: Input is a 20x20 array of characters (1s, 0s, and E) from an ASCII text data file(maze.txt); for example:
E0001110000000100100
11100011101110001111
11111000101000111000
00001110101100010010
01011000101111000110
00001110000110011110
11011100110110111000
00011110110111111101
01011011110110100001
01000000000110110111
11011011010010000000
01010010011000101011
01111000101110101110
00001110000111011001
01101011101101000011
11000110100111011010
01110000100100110011
11010111110110000000
01110100011000111110
00011001000011100010
Each data line consists of one row of maze. Starting points (i.e. a row, column pair) in the maze will be input from the keyboard.
Output: Echo print the maze complete with numbered rows and columns prior to asking the user for their starting point. For each entry into the maze, print the complete maze
with an S in the starting point followed by the words ‘I am free’ if you have found a path out of the maze or the words ‘Help, I am trapped’ if you cannot. You must also print the path (by using a series of pluses(+)) you took through the maze should one be found.
Here is a summary of algorithm:
1. At the beginning square we will examine the four adjacent squares and push onto the move stack the ones with a 0 or E in them.
2. In order to move, pop one square from the stack (Stack1). If stack is empty (when this happens it means that you are surrounded by 1s and/or +s), you are trapped. Mark the square we are in as having been visited by putting a + in the square.
3. Get the next move from the stack and make the square whose coordinates have just been popped the current square.
4. Repeat the process until you either reach the exit point or try to back track and the stack
5. Note that the above algorithm does not explain how one determines and marks the path to the exit from the starting point.
